- name: Enablement - Section PPI, Stage PPI - Median End User Page Load Time
  base_path: "/handbook/product/performance-indicators/"
  definition: Median end user page performance collected by Real User Monitoring. This metric
    captures how well we are doing in serving our primary customers, our end users,
    by measuring their end user experience.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  public: true
  pi_type: Section PPI, Stage PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
      - We have improved, but are still [slower than our primary competition](https://dashboards.gitlab.net/d/performance-comparison/github-gitlab-performance?orgId=1)
      - We are continuing to focus on LCP as an [OKR in Q4](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9213) for Q4 to continue to improve.
  implementation:
    status: Dashboard
    reasons:
    - Working with data team to calculate and dashboard the metric. Using [Page Load Time](https://developer.mozilla.org/en-US/docs/Web/API/Navigation_timing_API#Examples) as best metric available from Snowplow without additional instrumentation.
    - No target defined, as [metric is not dashboarded yet](https://gitlab.com/gitlab-data/analytics/-/issues/5657).
  lessons:
    learned:
    - We have made improvements to LCP, however [TTI/TBT are still significant gaps](https://gitlab.com/gitlab-com/Product/-/issues/1344#note_446025281) vs. our primary competition
    urls:
      - https://gitlab.com/gitlab-data/analytics/-/issues/5657
  metric_name: performanceTiming

- name: Enablement:Distribution - Group PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  section: enablement
  stage: enablement
  group: distribution
  public: true
  pi_type: Group PPI 
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Insights - We have trended down slightly in November, from 29% to 27%. This is largely being driven by our [Core instances](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8458214&udv=1059380), although we are seeing a more gradual decrease in Paid as well.
    - In order to achieve our target, we will need to reverse the broader trend.
  implementation:
    status: Complete
    reasons:
    - Primary PPI is complete, and target is set.
  lessons:
    learned:
    - There is a [continuing trend](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8458214&udv=1059380) towards more out of date instances, which has been continuing since 2017. The Postgres 11 upgrade in 13.0 was a brief bump, but did not significantly alter the long term trajectory. 
    - Even when controlling for [dormant instances with no activity](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=9069874&udv=1059380), the trend persists.
    - Before executing [potential solutions](https://gitlab.com/gitlab-org/gitlab/-/issues/15993), we should try to better understand the problem. We are running a survey to collect [installation feedback](https://gitlab.com/gitlab-org/gitlab/-/issues/268398), and will follow up with [updates](https://gitlab.com/gitlab-org/gitlab/-/issues/292432).
  metric_name: versions_behind_latest
  sisense_data:
    chart: 8658008
    dashboard: 406972
    embed: v2
  sisense_data_secondary:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10278985&udv=1102166
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10279895&udv=1102166

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric
    helps us understand whether end users are actually seeing value in, and are using,
    geo secondaries.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - First batch of data for users authenticating on a secondary via the WebUI is available; likely incomplete
    - We are working to add support for replicating all data types, so that Geo is on a solid foundation for both DR and Geo Replication. Snippet replication shipped in 13.7. [~80% of data types are now replicated](https://docs.gitlab.com/ee/administration/geo/replication/datatypes.html#limitations-on-replicationverification).
  implementation:
    status: Instrumentation
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com database.
    - Working towards tracking Git traffic on secondaries and replication events for other data via [Geo Usage Ping Epic](https://gitlab.com/groups/gitlab-org/-/epics/4660). We anticipate that most Geo users interact with Geo in this way.
  lessons:
    learned:
    - The number is very low right now. Two potential reasons a) data is incomplete b) WebUI is read-only and requires a different UI. We are planning to change this, see [this opportunity canvas](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit?usp=sharing)
    - Year over Year growth of [232% with regards to potential Geo users](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=9939914&udv=0)(17k to 58.5k). We know that Geo has low
      penetration as a percentage of total deployments, but skews heavily toward the large enterprise with a [25% percentage of Premium+ user share](https://docs.google.com/presentation/d/1imw_PWKZhJpxRs_VwTa-SWgwbZJhw2jKp6wMRk2Fo3c/edit#slide=id.g807f195cca_0_768).
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4660
  - https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics
  metric_name: geo_nodes
  sisense_data:
    chart: 10039214
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039565
    dashboard: 758607
    embed: v2    

- name: Enablement:Memory - Group PPI - Memory Consumed
  base_path: "/handbook/product/performance-indicators/"
  definition: Average memory consumed by all invididual GitLab processes
  target: 1.5GB
  org: Enablement Section
  section: enablement
  stage: enablement
  group: memory
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  implementation:
    status: Complete
    reasons:
    - Working to better support topology metrics for [multi-node instances](https://gitlab.com/groups/gitlab-org/-/epics/3576) and [Kubernetes](https://gitlab.com/groups/gitlab-org/-/epics/3577)
    - Working to adjust chart to include [how many of each process are started by default](https://gitlab.com/gitlab-com/Product/-/issues/1744) to better represent a default configuration.
  metric_name: topology.nodes[0].node_services
  sisense_data:
      chart: 10026240
      dashboard: 679200
      embed: v2
  health:
    level: 2
    reasons:
    - We are targeting to get below 2GB of total RAM usage, so we need to get these services [further below](https://gitlab.com/groups/gitlab-org/-/epics/448) that target.
    - We've identified [high-impact issues to reduce the overall memory footprint](https://gitlab.com/groups/gitlab-org/-/epics/448#estimated-memory-savings-by-issue)
    - Memory consumption is rising as a function of adding additional features

- name: Enablement:Global Search - Paid GMAU - The number of unique paid users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users and unique paid users interacting with either Basic Search or Advanced Search per month.
  target: 10% month over month (SaaS and self-managed combined)
  org: Enablement Section
  section: enablement
  stage: enablement
  group: global_search
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Heavy growth is expected for Self-managed as we see customers ramp up migrating to verson 13.4+
    - Growth is expected in SaaS as we have been adding new customers and features.
    - While we contiune to establish a baseline we are seeing other factors that indicate true growth is occuring.
      - 100 new namespaces to the advanced search SaaS index every 7 days.
      - We have a net new document count of 45,000 per hour in November. 
  implementation:
    status: Complete
    reasons:
    - Some data is not showing for SaaS Paid GMAU due to a [label change](https://gitlab.com/gitlab-data/analytics/-/issues/6683#note_437464387). SaaS GMAU started in September.
    - Self-managed data was implemented in 13.4.
  lessons:
   learned:
    - Search for files by name and path is surfacing as one of our largest Global Search use cases. 
    - Scaling Elasticsearch will continue to require engineering as long as we see growth in usage and document count
  sisense_data:
    chart: 10039566
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039216
    dashboard: 758607
    embed: v2

- name: Enablement:Database - Group PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: 0.99
  org: Enablement Section
  section: enablement
  stage: enablement
  group: database
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Apdex is currently trending in the wrong direction, although it is so close and only two datapoints, it could be noise. 
    - Improvement - We are focusing on [partitioning](https://gitlab.com/groups/gitlab-org/-/epics/2023) 
      the largest tables to improve the performance and scalability of the database.
  implementation:
    status: Complete
    reasons:
    - The [PPI has been instrumented](https://gitlab.com/gitlab-org/gitlab/-/issues/227305) in 13.4.
  lessons:
    learned:
    - Apdex is relatively stable over the last three releases.
    - Apdex does not seem to trend down based on scale of instance.
    - Apdex is notably lower on EE instances than CE, see secondary chart. We are investigating the reasons for this. There is no meaningful correlation with memory or PostgreSQL version. EE performs worse if the amount of queries is low (~1000 requests per hours) but overtakes CE for [very large request volumes](https://app.periscopedata.com/app/gitlab/754160/Enablement::Database---Performance-Indicators?widget=10102166&udv=0) (100k+ per hours). As we are dealing with averages this may indicate that some background queries dominate the average for lower request volumes but are drowned out at high volumes. 
    - Apdex on GitLab.com exceeds our group PPI (see [100ms - Tolerable 250ms](https://tinyurl.com/yxe4pv4a) and [50ms - Tolerable 100ms](https://tinyurl.com/y6latcuc))
  metric_name: query_apdex_weekly_average
  sisense_data:
    chart: 9885641
    dashboard: 754160
    embed: v2
  sisense_data_secondary:
    chart: 10091150
    dashboard: 754160
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305

- name: Enablement:Infrastructure - Paid GMAU - Number of unique paid users that perform
    an action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all monthly active users on gitlab.com that roll up to paid
    accounts in a 28 day rolling period.
  target: 111750
  org: Enablement Section
  section: enablement
  stage: enablement
  group: infrastructure
  public: true
  pi_type: Paid GMAU
  product_analytics_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Surpassed 100K Paid Monthly Active Users on GitLab.com in November
    - Insight - Growth rate decreased from 7.3% to 2.8% in November putting our Q4 target at risk. The decrease was largely driven by a drop in the Gold tier growth rate, which was higher than normal over the previous two months thanks to several new deal closures.
    - Improvement - Continued focus on improving overall availability of GitLab.com by resolving [corrective actions with stage groups](https://gitlab.com/groups/gitlab-org/-/boards/1193197?label_name[]=infradev), reducing complexity of deployments [by creating a centralized release controller](https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/20), and rolling out [Gitaly Cluster on .com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/363) to achieve increased redundancy of repositories.
  implementation:
    status: Complete
    reasons:
    - Instrumentation complete.
  lessons:
    learned:
    - Paid GMAU is a lagging indicator and changes month over month are best understood by looking at upstream activity. For example, the growth rate for the Gold tier can fluctuate significantly based on the number of signed deals for that tier in a given month. To obtain more visibility into upstream activity, we are planning to add a supporting metric, .com win rate, which will provide more visibility into our sales funnel. This effort is being [tracked here](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/1045)
  metric_name: COUNT_EVENTS_ACTIVE_USERS_LAST_28_DAYS_BY_PLAN_WAS_PAID
  sisense_data:
    chart: 9655306
    dashboard: 710777
    embed: v2
