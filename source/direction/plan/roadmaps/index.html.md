---
layout: markdown_page
title: "Category Direction - Roadmaps"
description: GitLab's vision is to provide robust, multi-level roadmaps that enable businesses to track current effort in flight and to to best utilize their resources.
canonical_path: "/direction/plan/roadmaps/"
---

- TOC
{:toc}

## Roadmaps

|                       |                               |
| -                     | -                             |
| Stage                 | [Plan](/direction/plan/)      |
| Maturity              | [Viable](/direction/maturity) |
| Content Last Reviewed | `2020-12-09`                  |

### Overview

#### Purpose

Large enterprises are continually working on increasingly more complex and larger scope initiatives that cut across multiple teams and departments, often spanning months, quarters, and even years. GitLab's vision is to provide robust, multi-level roadmaps that enable businesses to track current effort in flight, and plan upcoming work to best utilize their resources and focus on the right priorities. GitLab provides timeline-based roadmap visualizations to enable users plan from shorter delivery increments (e.g. 2 week sprints for development teams) to larger time scales (e.g. quarterly or annual strategic initiatives for entire departments). 

![roadmaps-direction.png](/direction/plan/roadmaps/roadmaps-direction.png)

#### Top Strategy Item(s)

To drive our vison for Roadmaps and how they supports our overall strategy for Portfolio Management we are continuing to develop the building blocks for deeper more expansive program and portfolio functionality. Currently we are validating and tracking the following big ticket items :

1. [Provide multiple configurable Roadmaps](https://gitlab.com/gitlab-org/gitlab/issues/3640)
1. [Enhance Roadmaps with Drag and Drop functionality](https://gitlab.com/gitlab-org/gitlab/issues/7725)
1. [Show Dependencies and Critical Dependency Paths on Roadmap](https://gitlab.com/gitlab-org/gitlab/issues/33587)


### What's next & why

To move towards our long term vision of robust roadmapping and planning, we are focusing on providing a solid foundation of functionality that we can build on in the future. We are working to ensure that the roadmap view provides the information you need to understand current state of work, identify blockers or bottlenecks, and ensure upcoming work is prepared for.

To begin, we will be releasing multiple enhancements to the current Roadmap view:

1. [Better Filtering on Roadmaps](https://gitlab.com/gitlab-org/gitlab/-/issues/212248)
1. [Surface Dependencies on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/33587)

### Maturity Plan

This category is currently at the **minimal** level, and our next maturity target is **viable** by 2021-01-31.

We are tracking progress against this target via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1998).

We are also tracking our progress toward **complete** via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2002).

### Competitive landscape

The Roadmapping space is large and is made up of stand alone tools, and as additional functionality on top of software planning systems like AHA.io, Roadmunk, Productboard, Monday.com, Trello, and Productplan. Many of these  were developed targeted at truly enterprise cases, allowing users to track large business initiatives across an organization. Customers using these tools often have another set of tools for the product-development teams to turn these high-level business initiatives into scoped out detailed planned work and actual software deliverables. Therefore, our competitive advantage is having _both_ (the high-level initiatives, and the product-development-level abstractions) in a single tool, that is fully integrated for a seamless experience. In addition to the enterprise oppritunity, we belive roadmapping can be also be beneficail to smaller organizations and individual teams to chart out the future and report on progress, so our strategy is building _toward_ those enterprise use cases starting with the product-development baseline abstractions. 


### Top User Issue(s)

As the usage of Roadmaps increase and more use cases and needs are being surfaced, we are tracking our most popular issues in the category. In particular, we are focusing on:

1. [Better Filtering on Roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/2923)
1. [Surface Dependencies on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/33587)

### Top Customer Success/Sales Issue(s)

To support our Customer Success and Sales departments, we are validating and working towards critical items to enable them to serve additional prospects and customers:

1. [Better Filtering on Roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/2923)
1. [Surface Dependencies on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/33587)
