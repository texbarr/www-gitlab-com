---
layout: markdown_page
title: "Category Direction - Quality Management"
description: View the GitLab category strategy for Requirements Management, part of the Plan stage's Requirements Management group.
canonical_path: "/direction/plan/quality_management/"
---

- TOC
{:toc}

## Quality Management
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

|                       |                                 |
| -                     | -                               |
| Stage                 | [Plan](/direction/plan/)        |
| Maturity              | [Planned](/direction/maturity/) |
| Content Last Reviewed | `2020-10-20`                    |

### Overview

Many organizations manage quality through both manual and automated testing. This testing is organized by test cases. These test cases can be run in different combinations and against different environments to create test sessions. Our goal for Quality management in GitLab is to allow for uses to track performance of test cases against their different environments over time, allowing for analysis of trends and identifiying critical failures prior to releasing to production.

We have performed a Solution Validation for the [Quality Management MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/208306).

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

The first step in building out Quality Management is a scaffolding framework for testing. In particular, we are calling these test cases, and test sessions. These will be first class native objects in GitLab, used to track the quality process of testing itself.

The MVC can be seen at [https://gitlab.com/groups/gitlab-org/-/epics/3852](https://gitlab.com/groups/gitlab-org/-/epics/3852). This work is currently ongoing, and we expect to have full support for test cases in GitLab 13.6, with additional support for test sessions in GitLab 13.7.

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Competitors in this space include qTest, Test Rail, and HPQC (HP Quality Center). They are focused on managing test cases as part of the software development lifecycle. Our approach and response will be to have similar basic test case management features (i.e. test objects), and then quickly move horizontally to integrate with other places in GitLab, such as issues and epics and even requirements management. See https://gitlab.com/groups/gitlab-org/-/epics/670. With this strategy, we would not be necessarily competing directly with these existing incumbents, but helping users with the integration pains of multiple tools and leveraging other, more mature areas of GitLab as we iterate.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We have yet to engage more closely with analysts in this area. As this product category is prioritized for improvements as our Plan product and engineering headcount grows, we expect to engage more with analysts.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

Few GitLab customers or prospects have asked about Quality Management. But the few that have, are asking about best practices and how they can possibly use GitLab and not worry with another tool. We are considering [https://gitlab.com/groups/gitlab-org/-/epics/617](https://gitlab.com/groups/gitlab-org/-/epics/617) for that very purpose.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

| Issue                                                                                                                          | 👍 |
| -                                                                                                                              | -  |
| [GitLab Quality Center](https://gitlab.com/gitlab-org/gitlab/issues/1538)                                                      | 36 |
| [Test case management and test tracking in a Native Continuous Delivery way](https://gitlab.com/gitlab-org/gitlab/issues/8766) | 10 |
| [Test cases and test suites](https://gitlab.com/gitlab-org/gitlab/issues/26097)                                                |  8 |


## Top dogfooding issues
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

We continue to work with GitLab's Quality Team to scope out the MVC of quality management. See [https://gitlab.com/gitlab-org/gitlab/-/issues/216149](hhttps://gitlab.com/gitlab-org/gitlab/-/issues/216149).

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->
The top Vision Item for this category is to release the [MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/216149). It is our goal that by performing careful solution validation, we will come out with an initial offering for this product category which matches well with our target customers.
