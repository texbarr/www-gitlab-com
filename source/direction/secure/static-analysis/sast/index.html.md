---
layout: secure_and_protect_direction
title: "Category Direction - Static Application Security Testing (SAST)"
description: "Static Application Security Testing (SAST) checks source code to find possible security vulnerabilities."
canonical_path: "/direction/secure/static-analysis/sast/"
---

- TOC
{:toc}

## Description

### Overview

Static Application Security Testing (SAST) checks source code to find possible security vulnerabilities. SAST helps developers identify weaknesses and security issues earlier in the software development lifecycle before code is deployed. SAST usually is performed when code is being submitted to a code repository. Think of it like spell check for security issues.

SAST is performed on source code or binary files and thus usually won't require code to be compiled, built, or deployed. However, this means that SAST cannot detect runtime or environment issues. SAST can analyze the control flow, the abstract syntax tree, how functions are invoked, and if there are information leaks to detect weak points that may lead to unintended behaviors.

Just like spell checkers, SAST analyzers are language and syntax specific and can only identify known classes of issues. SAST does not replace code reviewers, instead, it augments them, and provides another line of proactive defense against common and known classes of security issues. SAST is specifically about identifying potential security issues, so it should not be mistaken for [Code Quality](https://about.gitlab.com/direction/verify/code_quality/).

Security tools like SAST are best when integrated directly into the [Devops Lifecycle](https://about.gitlab.com/stages-devops-lifecycle/) and every project can benefit from SAST scans, which is why we include it in [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/). 

> GitLab was recently named as a [Niche player in the 2020 Gartner Quadrant for Application Security Testing](https://about.gitlab.com/analysts/gartner-ast20/). 


### SAST Direction Kickoff Playlist
Watch recent [GitLab Kickoffs](https://about.gitlab.com/handbook/product/product-processes/#kickoff-meetings) covering Static Analysis direction updates: 
<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/videoseries?list=PL05JrBw4t0KpG-Vf4OE_1ZcGCfl4hqoJ1" frameborder="0" allowfullscreen></iframe>
</figure>

### Goal

Overall we want to help developers write better code and worry less about common security mistakes. SAST should help *prevent* security vulnerabilities by helping developers easily *identify* common security issues as code is being contributed and *mitigate* proactively.  SAST should *integrate* seamlessly into a developer’s workflow because security tools that are actively used are effective.

* *Prevent* - find common security issues as code is being contributed and before it gets deployed to production.
* *Identify* - continuously monitor source code for known or common issues.
* *Mitigate* - make it easy to remediate identified issues, automatically if possible.
* *Integrate* - integrate with the rest of the DevOps pipeline and [play nice with other vendors](https://about.gitlab.com/handbook/product/gitlab-the-product/#plays-well-with-others).

The importance of these goals is validated by GitLab's [2020 DevSecOps Landscape Survey](https://about.gitlab.com/developer-survey/#security). With 3,650 respondents from 21 countries, the survey found: 

* Only 13% of companies give developers access to the results of application security tests.
* Over 42% said testing happens too late in the lifecycle. 
* 36% reported it was hard to understand, process, and fix any discovered vulnerabilities. 
* 31% found prioritizing vulnerability remediation an uphill battle.

> “GitLab Secure enables us to ship faster. Our other scanner tools could take up to a day to finish scanning whereas Secure scans finish as little a few minutes” - Healthcare services organization, GitLab Ultimate Customer

### Language Support
We want to make SAST easy to set up and use, making complexity transparent to users where possible. GitLab can automatically detect the programming language of a project and run the appropriate analyzer. We [support a variety of popular languages and frameworks](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks).

We want to increase language coverage by including support for the most common languages. We look at a variety of sources to determine language priorities including [industry trends](https://insights.stackoverflow.com/survey/2019#technology), [projects hosted on GitLab](https://about.gitlab.com/blog/2020/04/02/security-trends-in-gitlab-hosted-projects/), as well as [analyst reports](https://www.gartner.com/en/documents/3984345/magic-quadrant-for-application-security-testing) (italics below indicate languages called out specifically in analyst reports).

Language priorities (in addition to our existing [language support](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks)):
- *Java*
- *C#* (.NET Core & Framework)
- *PHP*
- *JavaScript* (better support for popular frameworks)
- Python
- Go
- Mobile (iOS & Android) - [Added in 13.6](https://about.gitlab.com/releases/2020/10/22/gitlab-13-5-released/#sast-support-for-ios-and-android-mobile-apps)

If we don't support a language you use, you can request support by [commenting on this epic](https://gitlab.com/groups/gitlab-org/-/epics/297) with details. 

We are also working on a [generic language-agnostic scanning](https://gitlab.com/groups/gitlab-org/-/epics/3260) approach. While currently experimental, generic scanning presents many opportunities to move faster and put more focus on the security rulesets rather than the implementation of those rules in various scanners. This will be a strategic focus for GitLab SAST leading into 2021. 

*User success metrics*
At GitLab, we [collect product usage data](https://docs.gitlab.com/ee/development/product-analytics/) to help us build a better product. You can see [growth of GitLab SAST on our performance indicators dashboard](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/#securestatic-analysis---gmau---users-running-static-analysis-jobs). 

The following metrics are also of interest as they help us know which area of SAST on which to focus:
- Tracking the # of SAST configurations (default, out of date, customized)
- Tracking the # of SAST jobs (increase coverage across repos)
- Tracking the # of issues identified by SAST
- Tracking the # of issues resolved that were identified by SAST
- Diversity of coverage of SAST jobs (language, type of identified issues, severity of issues)

## Roadmap
[The SAST Category Maturity level](https://about.gitlab.com/direction/maturity/#secure) is currently at `Viable`. We plan to mature it to `Complete` by mid 2021. 

 - [SAST Direction Epic](https://gitlab.com/groups/gitlab-org/-/epics/527)
 - Next Maturity Milestone Epic: [SAST to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2895)

### What's Next & Why
With all of our open-source based [SAST scanners now available in core for all GitLab users](https://about.gitlab.com/releases/2020/08/22/gitlab-13-3-released/#sast-security-analyzers-available-for-all), we are focused on increasing usability for non-ultimate customers. We're improving the [MR Security widget experience](https://gitlab.com/groups/gitlab-org/-/epics/4388) and [adding a SAST configuration tool](https://gitlab.com/gitlab-org/gitlab/-/issues/241377) for core users. We are also working on stability and support focused on [more advanced codebases like monorepo and multi-product git projects](https://gitlab.com/groups/gitlab-org/-/epics/4895). As part of these stability improvements, we are also looking to simplify some of the fundamental technical architecture of our product as we look towards the 14.0 release in May 2021 and [will be making some deprecations and removals](https://gitlab.com/gitlab-org/gitlab/-/issues/273620). 

We also are continuing our efforts with our [generic language-agnostic scanning](https://gitlab.com/groups/gitlab-org/-/epics/3260) approach. This will bring improvements to our vulnerability detection engine and help reduce false positives as well as [provide developers increased context for taking action](https://gitlab.com/gitlab-org/gitlab/-/issues/284337) to remediate SAST findings. These efforts will set us up to rapidly iterate in this space in early 2021. 

**Why is this important?**

In 2020, GitLab greatly expanded access to SAST to all our customers including our free tier users. Looking forward to 2021, we want GitLab SAST to be industry-leading with high-signal findings and low false-positive rates. SAST has a very real impact to help the world write better code. With wide accessibility to SAST and high-quality security data built directly into the DevSecOps workflow. GitLab will be able to leverage the context of not just repository source code, but also how projects are built, deployed, and changes over time. We'll be able to further integrate [all of our application tools](https://about.gitlab.com/stages-devops-lifecycle/secure/) to make them smarter, more accurate, and more automated. 

> “GitLab Secure allowed us to consolidate spend with centralized tools enabling a more streamlined workflow for our developers” - Retail product research organization, GitLab Ultimate Customer

**Differentiation**

Gitlab uniquely has opportunities within the entire DevOps lifecycle. We can integrate across different DevSecOps stages leveraging data, insight, and functionality from other steps to enrich and automate based on SAST findings.
We even allow [integration with partners and competitors](https://about.gitlab.com/partners/#security) to ensure flexibility. This allows teams to choose specific SAST solutions that fit their unique needs without GitLab being a constraint. This centers GitLab as the system of control and allows people to [extend and integrate other solutions](https://docs.gitlab.com/ee/development/integrations/secure.html) into the GitLab DevSecOps workflow.

## Maturity Plan

- [SAST to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2895)

## Recent Noteworthy Features
- [13.6 - Support for disabling pre-existing SAST rules](https://about.gitlab.com/releases/2020/11/22/gitlab-13-6-released/#support-for-disabling-pre-existing-sast-rules)
- [13.6 - New SAST severity data for Rails vulnerabilities](https://about.gitlab.com/releases/2020/11/22/gitlab-13-6-released/#new-sast-severity-data-for-rails-vulnerabilities)
- [13.5 - Customizing SAST & Secret Detection rules](https://about.gitlab.com/releases/2020/10/22/gitlab-13-5-released/#customizing-sast--secret-detection-rules)
- [13.5 - Improved SAST severity data for C/C++ and NodeJS vulnerabilities](https://about.gitlab.com/releases/2020/10/22/gitlab-13-5-released/#improved-sast-severity-data-for-cc-and-nodejs-vulnerabilities)
- [13.4 - Important SAST Deprecations](https://about.gitlab.com/blog/2020/08/19/gitlab-com-13-4-breaking-changes/)
- [13.3 - SAST available for all GitLab customers](https://about.gitlab.com/releases/2020/08/22/gitlab-13-3-released/#sast-security-analyzers-available-for-all)
- [13.3 - SAST configuration UI](https://about.gitlab.com/releases/2020/08/22/gitlab-13-3-released/#guided-sast-configuration-experience)
- [13.1 - SAST Support for Helm Charts](https://about.gitlab.com/releases/2020/06/22/gitlab-13-1-released/#sast-scanning-for-helm-charts)
- [13.0 - .Net Framework Support](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/#sast-for-net-framework)
- [12.10 - SAST Support for Air-gapped networks](https://about.gitlab.com/releases/2020/04/22/gitlab-12-10-released/#enhanced-secure-workflows-for-use-in-offline-environments)

[View the full changelog of SAST features](https://gitlab-cs-tools.gitlab.io/what-is-new-since/?selectedCategories=SAST)

## Competitive Landscape
Many well-known commercial products provide SAST solutions. Most of them support multiple languages and provide limited integration into the development lifecycle. 

Competitors are focused on a few areas:
* Accuracy, breadth, and scope of identifiable issues
* More code language support
* Detection Intelligence (AI/ML)
* Solution automation

Here are some vendors providing SAST tools:
* [Checkmarx](https://www.checkmarx.com/products/static-application-security-testing)
* [Synopsys](https://www.synopsys.com/software-integrity/security-testing/static-analysis-sast.html)
* [SonarQube](https://www.sonarqube.org)
* [GitHub Enterprise](https://github.com/features/security)
* [CA Veracode](https://www.veracode.com/products/binary-static-analysis-sast)
* [Fortify](https://software.microfocus.com/en-us/products/static-code-analysis-sast/overview)
* [IBM AppScan](https://www.ibm.com/security/application-security/appscan)

GitLab has a unique position to deeply integrate into the development lifecycle, with the ability to leverage CI/CD pipelines to perform the security tests. There is no need to connect the remote source code repository, or to use a different interface. GitLab is consistently now having enterprise customers replacing traditional Security scanning tools in favor of GitLab's fully integrated Security Scanning tools: 

> “GitLab Secure replaced Veracode, Checkmarx, and Fortify in my DevOps toolchain.  Secure scans faster, more accurate, and doesn’t require my developers to learn new tools” - Financial services organization, GitLab Gold Customer

We can improve the experience even further by supporting additional features that are currently present in other tools.

* [Support incremental scans for SAST](https://gitlab.com/gitlab-org/gitlab-ee/issues/9815)
* [Auto Remediation support for SAST](https://gitlab.com/gitlab-org/gitlab-ee/issues/9480)

## Analyst Landscape
We want to engage analysts to make them aware of the security features already available in GitLab. They also perform analysis of vendors in the space and have an eye on the future. We will blend analyst insights with what we hear from our customers, prospects, and the larger market as a whole to ensure we’re adapting as the landscape evolves. 

* [2020 Gartner Magic Quadrant: Application Security Testing, 29 April, 2020](https://about.gitlab.com/resources/report-gartner-mq-ast/)
* [2020 Fall G2 Leader: Best Static Application Security Testing (SAST) Software](https://www.g2.com/categories/static-application-security-testing-sast#grid), view full list of [public GitLab Reviews](https://www.g2.com/products/gitlab/reviews)
* [2020 Q3 Forrester Now Tech: Static Application Security Testing, 6 August, 2020](https://www.forrester.com/report/Now+Tech+Static+Application+Security+Testing+Q3+2020/-/E-RES161475?objectid=RES161475)
* [2020 GigaOm Radar for Evaluating DevSecOps Tools, 19 November, 2020](https://gigaom.com/report/gigaom-radar-for-evaluating-devsecops-tools/)
* [2020 Gartner Hype Cycle for Application Security, 27 July, 2020](https://www.gartner.com/document/3988043?ref=solrResearch&refval=260086210)
* [2020 Gartner Research: Structuring Application Security Tools and Practices for DevOps and DevSecOps, 18 June, 2020](https://www.gartner.com/doc/3986517?ref=solrResearch&refval=254019907)
* [2020 Gartner Resarch: 7 Tips to Set Up an Application Security Program Without Breaking the Bank, 11 June, 2020](https://www.gartner.com/document/3986206?ref=solrResearch&refval=254020933)
* [2020 Gartner Research: How to Deploy and Perform Application Security Testing, 20 March 2020](https://www.gartner.com/en/documents/3982363/how-to-deploy-and-perform-application-security-testing)
* [2020 GitLab DevSecOps Landscape Survey, 18 May, 2020](https://about.gitlab.com/developer-survey/)
* [2020 StackOverflow Developer Survey](https://insights.stackoverflow.com/survey/2020#technology-collaboration-tools)
* [Gartner Application Security Testing Reviews](https://www.gartner.com/reviews/market/application-security-testing)
* [OWASP SAST Tools](https://www.owasp.org/index.php/Source_Code_Analysis_Tools)

## Top Customer Success/Sales Issue(s)
* [Full list of SAST issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST)

## Top user issue(s)
* [Full list of SAST issues with Customer Label](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST&label_name[]=customer)
* [Full list of SAST issues with internal customer label](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST&label_name[]=internal%20customer)

## Top Direction Item(s)
* [SAST Product Vision](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASAST&label_name[]=direction)

## Related Categories
* [DAST](https://about.gitlab.com/direction/secure/dynamic-analysis/dast/) - Dynamic Application Security Testing
* [Dependency Scanning](https://about.gitlab.com/direction/secure/composition-analysis/dependency-scanning/) - Evaluating Dependency Vulnerabilities 
* [Vulnerability Management](https://about.gitlab.com/direction/secure/vulnerability_management/) - Security Dashboards, Reports, and interacting with Vulnerabilities

Last Reviewed: 2020-12-03

Last Updated: 2020-12-03
