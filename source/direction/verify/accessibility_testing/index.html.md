---
layout: markdown_page
title: "Category Direction - Accessibility Testing"
description: "Beyond being a compliance requirement in many cases, accessibility testing is the right thing to do for your users. Learn more here!"
canonical_path: "/direction/verify/accessibility_testing/"
---

- TOC
{:toc}

## Accessibility Testing

Beyond being a compliance requirement in many cases, [accessibility testing](https://about.gitlab.com/blog/2020/03/04/introducing-accessibility-testing-in-gitlab/) is the right thing to do for your users. Accessibility testing is similar to user acceptance testing (UAT) or usability testing. GitLab views accessibility as a primary concern on its own. Our vision for this category is to provide actionable data that can improve the accessibility and readability of your web application within GitLab.

For more information on Usability Testing at GitLab, visit the category [direction page](/direction/verify/usability_testing/). 

Interested in joining the conversation for this category? Please join us in the issues where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AAccessibility%20Testing)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

Up next for Accessibility Testing will be a [full view of the Accessibility report](https://gitlab.com/gitlab-org/gitlab/-/issues/36170) for a project. This is important for our Developer and Product Designer personas who want to understand outside of the context of the Merge Request what accessibility issues exist in a project.

We are also working on expanding use of Accessibility Testing and can acheive that goal by adding [Accessibility Testing to AutoDevOps](https://gitlab.com/gitlab-org/gitlab/-/issues/227172).

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is "Complete" (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Add Accessibility testing to AutoDevOps](https://gitlab.com/gitlab-org/gitlab/-/issues/227172)
- [Track accessibility results over time](https://gitlab.com/gitlab-org/gitlab/-/issues/36171)
- [Show a full accessibility report in GitLab](https://gitlab.com/gitlab-org/gitlab/issues/36170)
- [Add more accessibility scanners](https://gitlab.com/gitlab-org/gitlab/-/issues/218551)

We may find in research that only some of these issues are needed to move the vision for this category forward. The work to move the vision is captured and being tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/3491).

## Competitive Landscape

CI Competitors in this space are not providing first-party accessibility testing platforms. Many tools integrate with [pa11y](https://pa11y.org/), [lighthouse](https://github.com/GoogleChrome/lighthouse) or other tools to generate results via the CI/CD pipeline.

### Lighthouse CI
[Lighthouse CI](https://github.com/GoogleChrome/lighthouse-ci/blob/master/docs/getting-started.md) can provide a very rich report about accessibility and performance issues found in a scan of a website. It is straightforward to store the reports to be made accessible publicly for review outside of a development team. The data provided on the report offers a great actionable view of data and can be setup to fail a build if certain thresholds are not met. Where we think we can outperform this tool is by providing comparison between a change in progress and a previous report OR a threshold. This would allow a team to make progress even as they are still below a lofty target without failing a build. Another advantage we believe we provide is removing the need to run another server to store and view reports.

## Top Customer Success/Sales Issue(s)

The Field team is most interested in uptier features, which include [Premium](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AAccessibility%20Testing&label_name[]=GitLab%20Premium) and [Ultimate](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=GitLab%20Ultimate&label_name[]=Category%3AAccessibility%20Testing). As a new category, Accessibility Testing does not have a clear plan for uptier features at this time. 

## Top Customer Issue(s)

With the release of GitLab 12.8 the top customer item ([gitlab#25566](https://gitlab.com/gitlab-org/gitlab/issues/25566)) was addressed. We look forward to feedback on the issues being tracked in the current epic for the category and beyond from customers.

## Top Internal Customer Issue(s)

The Design team has an open accessibility epic that contains items about making GitLab itself more accessible: [gitlab-org#567](https://gitlab.com/groups/gitlab-org/-/epics/567). The long term vision for this category is for that team to be able to use GitLab to detect all of those issues in an automated way and see that they are addressed when fixed.

Another interesting issue is one to add a [scan for a readability](https://gitlab.com/gitlab-org/gitlab/-/issues/224607) which can be used to ensure heavy text sites, like the GitLab handbook and Documentation, are easily readable. Text that is easy to understand is also easier to translate and results in a more easily understood translated version.

## Top Vision Item(s)

To meet our long term vision we will need to solve problems that will expand our out-of-the-box accessibility testing like helping customers track how [accessibility is changing over time](https://gitlab.com/gitlab-org/gitlab/issues/36171) and [providing additional scanners](https://gitlab.com/gitlab-org/gitlab/-/issues/218551) so users can be confident their web app is accessible by everyone.
