---
layout: markdown_page
title: How we care for our customers
description: "At GitLab, we consider customers as part of our greater community. It's important to consider that our focus is two-fold."
canonical_path: "/customer-care/"
---

## Customers in Context of the Greater Community

At GitLab, we consider customers as part of our greater community. It's important 
to consider that our focus is two-fold:

+ Increase community involvement and access to GitLab products and culture
+ Provide value to customers

As we work to provide value for our customers, there are two specific departments 
that will be highly involved and focused on the customer throughout their lifecycle:

- Customer Success
- Support

This page serves as a primer as to the basics of those groups and access to greater 
specific materials related to both.

## Customer Success

Customer Success is comprised of early and late stage teams to help the customer 
fully realize the potential of GitLab in their environment. From Solutions Architects 
to Technical Account Managers GitLab's Customer Success team is focused on:

- Identifying the Customer's Needs
- Scoping out Potential Solutions
- Guiding and Implementing GitLab Installations
- Identifying new features and workflows that help the customer achieve their specific goals

You can read more about specific processes and sub-groups inside of Customer 
Success in our [Customer Success Handbook](/handbook/customer-success/)

## Support

As customers deploy and use GitLab, they may run into configuration, scaling problems, 
or defects. These type of problems are managed by the Support Team at GitLab. Support at 
GitLab is focused on the following:

- Identifying unknown defects
- Adding field notes to known issues
- Writing code fixes (found in MRs with the ~"Support Team Contributions" label)
- Clarifying Documentation based on customer feedback/exposure
- Coordinating with Customer Success to understand specific customer needs

You can read more about the Support function and specific processes used in the [Support Handbook](/handbook/support/)

These two groups will work together to help the customer realize the value of choosing and using GitLab.