---
layout: handbook-page-toc
title: "Handbook error mash"
description: "Let's fix handbook errors together!"
noindex: true
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Handbook error mash
{:.no_toc}

As we get close to the end of spooky season, the Inbound Marketing team wants to ask for your help to fix SEO errors across the handbook. For the next two weeks, we're asking teams to look at their section of the handbook and fix duplicate or missing page titles, descriptions, and missing pages. 

To help you get started the Search Marketing team recorded a few videos to show how to fix these types of errors and collected errors in a Google Sheet.

As you fix errors, please add the `~handbook-error-mash` label so we can track how many MRs we complete in this iteration and help us improve as we do future iterations of this error mash.

## Handbook errors Google Sheet

The [Handbook Site Health](https://docs.google.com/spreadsheets/d/17xgjt9jvnf71dA8YHDwD84zrJMQoid012Ot0aYU0igU/edit#gid=470641629) sheet has three tabs, `Duplicate Titles`, `Missing Meta Descriptions`, and `Broken Internal Links`. 

The title and description tabs both have the page with an error, team, and a checkbox to make an error as fixed.

The broken link tab lists the link location (where a broken link occurs), the broken link, and the link text. There's also a team listed, and checkbox for completion.

## Videos

The Search Marketing team made a series of videos showing why fixing these errors will help GitLab's SEO and how to fix each type of issue in the WebIDE. You can find all of these in the [2020 Handbook error mash](https://www.youtube.com/playlist?list=PL05JrBw4t0Kob8oW0viUnFPOvHk_Ki4EX) playlist on GitLab Unfiltered.

### Handbook error mash kickoff video

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/U1xr1vq4Vfs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Fixing page titles and meta descriptions

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SOOYUiRwZik" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hcw63x5TGGk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Broken links and 301 redirects

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/W40ciCKtzpk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

While you're fixing those broken links, consider [making the link text meaningful](/handbook/markdown-guide/#links) if it isn't already. This helps to improve our site's accessibility for people who use screen readers.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/1K8irzpGExY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/VE1gpOzyufs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
