---
layout: handbook-page-toc
title: "Pricing Analysis"
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

## Pricing Analysis

Pricing is the process of analyzing the value customers receive from GitLab at specific price points along with the profitability of those price points. The analysis also includes understanding how these prices affect the overall business and determining what the optimal price points are for customers and GitLab's profitability. The Pricing Analysis page will provide the information and tools that GitLab team members can use to explore our current pricing strategy and develop insights to optimize it.

This data solution delivers three [Self-Service Data](/handbook/business-ops/data-team/direction/self-service/) capabilities:

1. Dashboard Viewer - a series of new SiSense dashboards to visualize customers discounts, renewals, customers counts by different segments and more. (Self-Service Dashboard)
1. Dashboard Developer - a new SiSense data model containing the complete dimensional model components to build new dashboards and link existing dashboards to pricing analysis data.
1. SQL Developer - an Enterprise Dimensional Model subject area

From a Data Platform perspective, the solution delivers:

1. An extension to the Enterprise Dimensional Model for Pricing Analysis
1. Testing and data validation extensions to the Data Pipeline Health dashboard
1. ERDs, dbt models, and related platform components

### Quick Links
<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://app.periscopedata.com/app/gitlab/768077/Pricing-Dashboards---Customer-Discounts" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Pricing Discounts Dashboard</a>
  <a href="https://app.periscopedata.com/app/gitlab/735150/PnP-Analysis,-Test-and-Research" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">PnP Test and Research Dashboard</a>
  <a href="https://app.periscopedata.com/app/gitlab/748119/Pricing---Customer-Overview" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Pricing - Customer Overview Dashboard</a>
  <a href="https://www.youtube.com/watch?v=F4FwRcKb95w&feature=youtu.be" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Getting started using SiSense Discovery</a>
</div>
<br><br><br><br><br><br><br><br><br>

### Data Security Classification

Much of the data within and supporting Pricing Analysis is [Orange](/handbook/engineering/security/data-classification-standard.html#orange) or [Yellow](/handbook/engineering/security/data-classification-standard.html#yellow). This includes ORANGE customer metadata from the account and GitLab's Non public financial information, all of which shouldn't be publicly available. Care should be taken when sharing data from this dashboard to ensure that the detail stays within GitLab as an organization and that appropriate approvals are given for any external sharing. In addition, when working with row or record level customer metadata care should always be taken to avoid saving any data on personal devices or laptops. This data should remain in [Snowflake](/handbook/business-ops/data-team/platform/#data-warehouse) and [Sisense](/handbook/business-ops/data-team/platform/periscope/) and should ideally be shared only through those applications unless otherwise approved. 

**ORANGE**

- Description: Customer and Personal data at the row or record level.
- Objects:
  - `dim_billing_accounts`
  - `dim_crm_accounts`

**YELLOW**

- Description: GitLab Financial data, which includes aggregations or totals.
- Objects:
  - `dim_subscriptions`
  - `fct_charges`
  - `fct_invoice_items`
  - `fct_mrr`

### Solution Ownership

- Source System Owner:
    - Salesforce: `@jbrennan1`
    - Zuora: `@andrew_murray`
- Source System Subject Matter Expert:
    - Salesforce: `@jbrennan1`
    - Zuora: `@andrew_murray`
- Data Team Subject Matter Expert: `@paul_armstrong` `@jeanpeguero` `@jjstark` `@iweeks`

### Key Terms

1. [Product Category, Product Tier, Delivery](https://about.gitlab.com/handbook/marketing/strategic-marketing/tiers/#overview)
1. [Sales Segment](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation)
1. [Account Owner Team](https://about.gitlab.com/handbook/sales/#initial-account-owner---based-on-segment)
1. [Customer](https://about.gitlab.com/handbook/sales/sales-term-glossary/#customer)

### Key Metrics, KPIs, and PIs

1. [ARR](https://about.gitlab.com/handbook/sales/sales-term-glossary)
1. [ARPU](https://about.gitlab.com/handbook/sales/sales-term-glossary/#revenue-per-licensed-user-also-known-as-arpu-or-arpa)
1. [Licensed Users](https://about.gitlab.com/handbook/sales/sales-term-glossary/#licensed-users)

## Self-Service Data Solution

### Self-Service Dashboard Developer

A great way to get started building charts in Sisense is to watch this 10 minute [Data Onboarding Video](https://www.youtube.com/watch?v=F4FwRcKb95w&feature=youtu.be) from Sisense. After you have built your dashboard, you will want to be able to easily find it again. Topics are a great way to organize dashboards in one place and find them easily. You can add a topic by clicking the `add to topics` icon in the top right of the dashboard. A dashboard can be added to more than one topic that it is relevant for. Some topics include Finance, Marketing, Sales, Product, Engineering, and Growth to name a few. 

### Self-Service SQL Developer

#### Key Fields and Business Logic

- Data is sourced from Zuora and Salesforce.
- Percent Discount = ( Sum(ARR List Price) - Sum(ARR) ) / Sum(ARR List Price ) 
- ARR List Price = Annual Product List Price * Number of Seats
- Resellers are identified at the charge level by looking at the `dim_crm_account_id_invoice` of the invoice. Determining if a subscription is from a reseller or not can only be done by looking at the invoices.
- `Reporter`, `Education`, `OSS`,`Y combinator`,`Support` and `Guests` product charges can be removed from `Mart_Discount_ARR` by using the `is_excluded_from_disc_analysis=TRUE` flag as can be seen [HERE](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/marts/arr/mart_discount_arr.sql#L93).
- Self-Serve and Sales Assisted subscriptions are differenciated by looking at its `created_by_id` as can be seen [HERE](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/models/marts/arr/mart_discount_arr.sql#L96).


#### Entity Relationship Diagrams

| Diagram/Entity  | Purpose | Keywords |
| ---- | ----- | ------- | -------- |
| [Mart Discount ARR](https://lucid.app/lucidchart/invitations/accept/405205e2-b25e-4f32-9345-43dd466b2564) | Provide insights into discounts and reseller deals by various customer dimensions | Parent Customer, Product Category, Delivery, Account Owner Team, Reseller, Subscription Name, Accounts    |

#### Reference SQL

| Snippet Library   | Description |
| ---- | ----   |
| [Pricing Analysis SQL Scrip](https://app.periscopedata.com/app/gitlab/snippet/base_pricing_discount/55af4b76941044829e019f6b0e29877a/edit)      |  Query to slice customer discounts by Reseller, ARR and Number of Seats buckets, Sales Segment, Account Owner Team, Product Category and more.          | 

## Data Platform Solution

### Data Lineage

- Data is sourced from Salesforce.com and Zuora, exluding accounts from manually managed list of [zuora excluded accounts](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/data/zuora_excluded_accounts.csv)
- Complete data lineages can be found at [dbt mart_discount_arr lineage chart](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.mart_discount_arr?g_v=1&g_i=%2Bmart_discount_arr%2B), [dbt mart_arr lineage chart](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.mart_arr?g_v=1&g_i=%2Bmart_arr%2B)

### DBT Solution

The dbt solution generates a dimensional model from RAW source data. 
The exceptions are the following fields that are calculated based on business logic implemented within specific dbt models:

| field | business logic |
| ------ | ------ |
| ARR buckets       |  Calculated based of [ARR](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/macros/common/arr_buckets.sql)       |
| Number of Seats buckets | Calculated based of [Number of Seats purchased](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/macros/common/number_of_seats_buckets.sql) |

## Trusted Data Solution

See overview at [Trusted Data Framework](https://about.gitlab.com/handbook/business-ops/data-team/direction/trusted-data/)

[dbt guide examples](https://about.gitlab.com/handbook/business-ops/data-team/platform/dbt-guide/#trusted-data-framework) for
details and examples on implementing further tests


### EDM Enterprise Dimensional Model Validations

* [(WIP) Enterprise Dimensional Model Validation Dashboard](https://app.periscopedata.com/app/gitlab/760445/WIP:-Enterprise-Dimensional-Model-Validation-Dashboard)
    * Reports on latest Enterprise Dimensional model test and runs 

### RAW Source Data Pipeline validations

[Data Pipeline Health Validations](https://app.periscopedata.com/app/gitlab/715938/Data-Pipeline-Health-Dashboard)
