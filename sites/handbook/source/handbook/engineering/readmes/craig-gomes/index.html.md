---
layout: markdown_page
title: "Craig Gomes' README"
job: "Engineering Manager (Memory/Database)"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### My Manager Readme

I wrote my first manager readme a couple of years ago when I read about the [Manager Readme](https://managerreadme.com/) site in the [Software Lead Weekly](https://softwareleadweekly.com/) weekly email.  The site has great tools and templates to start with if you want some inspiration.  You can read my public readme [here](https://managerreadme.com/readme/craiggomes). Read on below.

### My role

My role is to foster engagement and empowerment.  Engaged teams and individuals are excited about what they do.  Empowered teams and individuals will make bold decisions and ultimately a successful product.

### What do I value most

I value honesty, direct communication and authenticity.  It can be difficult to be all three of these in the workplace these days.  What is direct communication for you could be offensive to someone else.  I've found the best way to understand how your colleagues like to receive communication is to simply ask.  What's their communication preference?  What's a trigger for them?  How do they like to receive critical feedback?  How to they prefer to receive praise?

My communication preference is to be direct with me.  Feed me the facts, details and suggestions on how to proceed.

My triggers are complaining without offering or brainstorming solutions and backstabbing.

I prefer critical feedback like regular communication; give me the details and suggestions on how to proceed.  

When it comes to receiving praise, I'm ok with letting the originator choose the method although they should tend more towards low-key praise.

### My Expectations

My main expectation is that people hold themselves accountable.  We have goals as individuals, as teams and as a company.  Set your goals, communicate them with your team and most importantly reflect on them.  Even with the successful accomplishment of goals, there are always learning opportunities.

In that same vein, hold me accountable to what I have promised.  At a minimum, I should follow through on progress if I can't meet my promises and set new expectations.

### 1:1s

1:1s should be led by you, and not me.  The frequency, duration and topics should largely be driven by you as well.  When I first set up our 1:1s I will ask you what works best for you and do my best to accommodate.   The main content of the 1:1 should also be driven by you.  I won't have a recurring agenda or template that we need to follow.  I will want to discuss what's most important to you and your career.  

When I have topics that I need to share with you I will give you as much advanced notice as I can.

### Personality quirks

One personality quirk that has been mentioned frequently is that I'm fairly quiet and when I do talk I can be terse.  I have even earned the nickname "Groot" because of this.

Also, when I write I use bullet points

- a
- lot
