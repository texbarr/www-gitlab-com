---
layout: handbook-page-toc
title: "RM.1.01 - Risk Assessment Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# RM.1.01 - Risk Assessment

## Control Statement

GitLab performs an annual security operational risk assessment to identify risks impacting security. The assessment considers both internal and external security risk factors, considerations over fraud and the impact of changes to the system. Results from the assessment are compiled into an annual report which is reviewed by management. Identified security risks are risk rated, assigned to a risk owner, and tracked through to risk treatment completion or risk acceptance.

## Context

Security operational risk assessments are important because they identify, prioritize, and help track the treatment of secruity risks to GitLab. The purpose of this control is to have a formalized annual security operational risk assessment process for risk identification, analysis, and treatment.

## Scope

The security operational risk assessment is performed at a level of precision that allows for the identification of security risks across the organization.

## Ownership

* Control Owner: `Security Compliance`
* Process owner(s): `Security Compliance`

## Guidance

The annual security operational risk assessment is performed in accordance with GitLab's [STORM Policy](/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html) and [Methodology](/handbook/engineering/security/security-assurance/risk-field-security/operational-risk-management-methodology.html).

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Risk Assessment control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/866).

Examples of evidence an auditor might request to satisfy this control:

* A copy of GitLab's security operational risk management policy and methodology.
* A copy of the non-confidential annual assessment summary
* A screenshot of the risk treatment folder, showing risk treatment plans that are active against the total risks in the Risk Register

### Policy Reference

* [STORM Policy](/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html)
* [STORM Methodology](/handbook/engineering/security/security-assurance/risk-field-security/operational-risk-management-methodology.html)

## Framework Mapping

* SOC2 CC
  * CC2.1
  * CC3.1
  * CC3.3
  * CC3.4
  * CC5.1 
